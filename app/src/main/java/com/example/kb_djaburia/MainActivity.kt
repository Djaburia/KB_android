package com.example.kb_djaburia

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.Window

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onAboutPage (view: View){
        startActivity(Intent(this,SecondActivity::class.java))
    }
    fun onContactsPage (view: View){
        startActivity(Intent(this,ThirdActivity::class.java))
    }
}